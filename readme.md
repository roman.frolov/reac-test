Тестовое задание на позицию React Frontend разработчика. Погодное приложение 
----
Описание задачи
----
Разработать прототип приложения показывающего погоду в выбранных городах.

Данные можно взять из бесплатной версии сервиса:
https://openweathermap.org/api
либо другого по желанию.

Приложение должено позволять добавлять/удалять города. 
Выбранные пользователем города должны запоминаться локально.

Каждый город рисовать отдельной карточкой. Карточка должна содержать информацию о:
 - Температуре воздуха
 - Скорости и направлении ветра
 - Давлении
 - Типе погоды

Для отображения типа погоды помимо названия использовать иконки.
Например вот такие:
http://erikflowers.github.io/weather-icons/

доп. задание 1:
----
Реализовать поиск и отображение изображений для выбраного города в качестве бэкграунда карточки с этим городом.
Если найти не удалось использовать какое-нибудь дефолтное изображение.
Сами данные отрисовывать на полупрозрачной подложке.
Картинки можно брать от сюда:
http://developers.teleport.org/api/
либо из другого сервиса по желанию.

доп. задание 2:
----
Найти сервис для отображения времени в каждом из выбранных городов.
Реализовать фильтрацию городов по указаным параметрам температуры и ветра 


Требование к решению:
----
- ReactJS + Redux
- адаптивная верстка при помощи sass или less на выбор, но НЕ bootstrap 
- настройка сборки при помощи webpack
- результат решения залить на github и прислать ссылку на репозиторий на r.frolov@whatwhere.world
